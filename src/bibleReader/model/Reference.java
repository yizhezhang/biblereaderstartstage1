package bibleReader.model;

// TODO Add Javadoc comments throughout the class.

/**
 * A simple class that stores the book, chapter number, and verse number.
 * 
 * @author Charles Cusack (Skeleton Code), 2008, edited 2012.
 * @author Yizhe Zhang (provided the implementation)
 */
public class Reference implements Comparable<Reference> {
	/*
	 * Add the appropriate fields and implement the methods. Notice that there are
	 * no setters. This is intentional since a reference shouldn't change.
	 * Important: You should NOT store the book as a string. There are several
	 * reasons for this that will be clear if you think about it for a few minutes.
	 * There is already a class that you can use instead.
	 */

	// TODO Add fields: Stage 2
	private BookOfBible book;
	private int chapter;
	private int verse;

	/**
	 * Create a new Reference with the given book, chapter, and verse. If book is
	 * not a valid abbreviation of a BookOfBible object, the book will be set to
	 * null.
	 * 
	 * @param book
	 * @param chapter
	 * @param verse
	 */
	public Reference(String book, int chapter, int verse) {
		this.book = BookOfBible.getBookOfBible(book);
		this.chapter = chapter;
		this.verse = verse;
	}

	public Reference(BookOfBible book, int chapter, int verse) {
		this.book = book;
		this.chapter = chapter;
		this.verse = verse;
	}

	public String getBook() {
		return book.toString();
	}

	public BookOfBible getBookOfBible() {
		return book;
	}

	public int getChapter() {
		return chapter;
	}

	public int getVerse() {
		return verse;
	}

	/**
	 * This method should return the reference in the form: "book chapter:verse" For
	 * instance, "Genesis 2:3".
	 */
	@Override
	public String toString() {
		return getBook() + " " + chapter + ":" + verse;
	}

	@Override
	public boolean equals(Object other) {
		if (other instanceof Reference) {
			return (((Reference) other).getBookOfBible() == book) && ((Reference) other).getChapter() == chapter
					&& ((Reference) other).getVerse() == verse;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return toString().hashCode();
	}

	@Override
	public int compareTo(Reference otherRef) {
		if(otherRef.getBookOfBible()!=book) {
			return book.compareTo(otherRef.getBookOfBible());
		}
		else if (otherRef.getChapter()!=chapter){
			return chapter-otherRef.getChapter();
		}
		else if (otherRef.getVerse()!=verse) {
			return verse - otherRef.getVerse();
		}
		return 0;
	}
}
