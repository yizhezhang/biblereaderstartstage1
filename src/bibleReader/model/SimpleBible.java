package bibleReader.model;

import java.util.ArrayList;

/**
 * A class that stores a version of the Bible.
 * 
 * @author Chuck Cusack (Provided the interface)
 * @author Yizhe Zhang (provided the implementation)
 */
public class SimpleBible {
	private ArrayList<Verse> verses;
	private String version;
	private String title;
	private int find;

	/**
	 * Create a new Bible with the given verses and sets the version and title.
	 * 
	 * @param verses
	 *            A VerseList of the verses of this version of the Bible whose
	 *            version is set to the version of the Bible they came from and
	 *            whose description is set to the title of the Bible.
	 */
	public SimpleBible(VerseList verses) {
		this.verses = new ArrayList(verses);
		version = verses.getVersion();
		title = verses.getDescription();
		find = -1;
	}

	/**
	 * Returns the number of verses that this Bible is currently storing.
	 * 
	 * @return the number of verses in the Bible.
	 */
	public int getNumberOfVerses() {
		return verses.size();
	}

	/**
	 * Returns which version this object is storing (e.g. ESV, KJV)
	 * 
	 * @return A string representation of the version.
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * Returns the title of this version of the Bible.
	 * 
	 * @return A string representation of the title.
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param ref
	 *            the reference to look up
	 * @return true if and only if ref is actually in this Bible
	 */
	public boolean isValid(Reference ref) {
		for (Verse i : verses) {
			if (ref.equals(i.getReference())) {
				find = verses.indexOf(i);
				return true;
			}
		}
		return false;
	}

	/**
	 * Return the text of the verse with the given reference
	 * 
	 * @param ref
	 *            the Reference to the desired verse.
	 * @return A string representation of the text of the verse or null if the
	 *         Reference is invalid.
	 */
	public String getVerseText(Reference ref) {
		if (isValid(ref) && find >= 0) {
			String x = verses.get(find).getText();
			find = -1;
			return x;
		}
		return null;
	}

	/**
	 * Return a Verse object for the corresponding Reference.
	 * 
	 * @param ref
	 *            A reference to the desired verse.
	 * @return A Verse object which has Reference r, or null if the Reference is
	 *         invalid.
	 */
	public Verse getVerse(Reference ref) {
		if (isValid(ref)) {
			for (Verse i : verses) {
				if (ref.equals(i.getReference())) {
					return i;
				}
			}
		}
		return null;
	}

	/**
	 * @param book
	 *            the book of the Bible
	 * @param chapter
	 *            the chapter of the book
	 * @param verse
	 *            the verse within the chapter
	 * @return the verse object with reference "book chapter:verse", or null if the
	 *         reference is invalid.
	 */
	public Verse getVerse(BookOfBible book, int chapter, int verse) {
		// TODO Implement me: Stage 2
		Reference ref = new Reference(book, chapter, verse);
		return getVerse(ref);
	}
}
