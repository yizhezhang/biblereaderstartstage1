package bibleReader.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import bibleReader.model.BookOfBible;
import bibleReader.model.Reference;
import bibleReader.model.Verse;

/*
 * Tests for the Verse class.
 * @author Jason Gombas and Yizhe Zhang
 */

public class Stage01StudentVerseTest {

	private Reference reference;
	private Reference reference2;
	private String text;
	private String book;
	private String text2;
	private int chapter;
	private int verseNo;

	@Before
	public void setUp() throws Exception {
		chapter = 1;
		verseNo = 2;
		reference = new Reference(BookOfBible.Genesis, chapter, verseNo);
		reference2 = new Reference(BookOfBible.John, chapter, verseNo);
		text = "Hello there";
		book = "Genesis";
		text2 = "See you";
	}

	@After
	public void tearDown() throws Exception {
	}

	// Test the constructor with input of reference and String
	public void testVerseReferenceString() {
		Verse verse = new Verse(reference, text);
		assertTrue(verse.getReference().equals(reference));
		assertTrue(verse.getText().equals(text));
	}

	@Test
	// Test the constructor with input of BookOfBible, int, int, String
	public void testVerseBookOfBibleintintString() {
		Verse verse = new Verse(BookOfBible.Genesis, chapter, verseNo, text);
		assertTrue(verse.getReference().equals(reference));
		assertTrue(verse.getText().equals(text));
	}

	@Test
	// Test the constructor with input of String, int, int, String
	public void testVerseStringintintString() {
		Verse verse = new Verse(book, chapter, verseNo, text);
		assertTrue(verse.getReference().equals(reference));
		assertTrue(verse.getText().equals(text));
	}

	@Test
	// Test the getReference method
	// Assuming equals method in reference work
	public void testGetReference() {
		Verse verse = new Verse(reference, text);
		Reference testReference = verse.getReference();
		assertTrue(testReference.equals(reference));
	}

	@Test
	// Test the getText method see if return the exact right string
	public void testGetText() {
		Verse verse = new Verse(reference, text);
		String testText = verse.getText();
		assertTrue(testText.equals(text));
	}

	@Test
	// Test the toString method see if return the correct String
	// Assuming tooString method in reference class work
	public void testToString() {
		Verse verse = new Verse(reference, text);
		String testString = verse.toString();
		String trueString = reference.toString() + " " + text;
		assertTrue(testString.equals(trueString));
	}

	@Test
	// Test the equals method when it suppose to return true
	public void testEqualsTrue() {
		Verse verse1 = new Verse(reference, text);
		Verse verse2 = new Verse(reference, text);
		assertTrue(verse1.equals(verse2));
	}

	@Test
	// Test the equals method when it suppose to return false
	public void testEqualsFalse() {
		Verse verse1 = new Verse(reference, text);
		Verse verse2 = new Verse(reference2, text);
		assertFalse(verse1.equals(verse2));
		Verse verse3 = new Verse(reference, text);
		Verse verse4 = new Verse(reference, text2);
		assertFalse(verse3.equals(verse4));
	}

	@Test
	// Test HashCode method when two object suppose to have equal hash code
	public void testHashCodeEqual() {
		Verse verse1 = new Verse(reference, text);
		Verse verse2 = new Verse(reference, text);
		assertTrue(verse1.hashCode() == verse2.hashCode());
	}

	@Test
	// Test HashCode method when two object suppose not to have equal hash code
	public void testHashCodeNotEqual() {
		Verse verse1 = new Verse(reference, text);
		Verse verse2 = new Verse(reference2, text);
		assertTrue(verse1.hashCode() != verse2.hashCode());
	}

	@Test
	// Test compareTo method when it suppose to return a negative value
	// Assuming Verse will have a reference field and compareTo method in reference
	// works
	// So no need test detail about book chapter and verse number
	public void testCompareToSmaller() {
		Verse verse1 = new Verse(reference, text);
		Verse verse2 = new Verse(reference2, text);
		assertTrue(verse1.compareTo(verse2) < 0);
	}

	@Test
	// Test compareTo method when it suppose to return a zero
	// Assuming Verse will have a reference field and compareTo method in reference
	// works
	// So no need test detail about book chapter and verse number
	public void testCompareToEqual() {
		Verse verse1 = new Verse(reference, text);
		Verse verse2 = new Verse(reference, text);
		assertTrue(verse1.compareTo(verse2) == 0);
	}

	@Test
	// Test compareTo method when it suppose to return a positive value
	// Assuming Verse will have a reference field and compareTo method in reference
	// works
	// So no need test detail about book chapter and verse number
	public void testCompareToGreater() {
		Verse verse1 = new Verse(reference2, text);
		Verse verse2 = new Verse(reference, text);
		assertTrue(verse1.compareTo(verse2) > 0);
	}

	@Test
	// Test sameReference method when it suppose to return true
	public void testSameReferenceTrue() {
		Verse verse1 = new Verse(reference, text);
		Verse verse2 = new Verse(reference, text2);
		assertTrue(verse1.sameReference(verse2));
	}

	@Test
	// Test sameReference method when it suppose to return False
	public void testSameReferenceFalse() {
		Verse verse1 = new Verse(reference, text);
		Verse verse2 = new Verse(reference2, text);
		assertFalse(verse1.sameReference(verse2));
	}

}
