package bibleReader.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import bibleReader.model.BookOfBible;
import bibleReader.model.Reference;

/*
 * Tests for the Reference class.
 * @author Jason Gombas
 * @author Yizhe Zhang
 */

public class Stage01StudentReferenceTest {

	// Tests for references with one thing in common
	private Reference gen1_5;
	private Reference gen2_6;
	private Reference gen5_2;
	private Reference rev5_3;
	private Reference gen2_10;
	private Reference rev3_10;
	// Tests for references with one thing different
	private Reference gen1_6;
	private Reference rev1_6;
	private Reference rev6_2;
	private Reference rev7_2;
	private Reference rev2_10;
	private Reference rev2_12;
	// Tests for everything different
	private Reference gen10_5;
	private Reference rev11_1;
	
	@Before
	public void setUp() throws Exception {
		gen1_5 = new Reference("Genesis", 1, 5);
		gen2_6 = new Reference("gen", 2, 6);
		gen5_2 = new Reference(BookOfBible.Genesis, 5, 2);
		rev5_3 = new Reference(BookOfBible.Revelation, 5, 3);
		gen2_10 = new Reference(BookOfBible.Genesis, 2, 10);
		rev3_10 = new Reference(BookOfBible.Revelation, 3, 10);

		gen1_6 = new Reference(BookOfBible.Genesis, 1, 6);
		rev1_6 = new Reference(BookOfBible.Revelation, 1, 6);
		rev6_2 = new Reference(BookOfBible.Revelation, 6, 2);
		rev7_2 = new Reference(BookOfBible.Revelation, 7, 2);
		rev2_10 = new Reference(BookOfBible.Revelation, 2, 10);
		rev2_12 = new Reference(BookOfBible.Revelation, 2, 12);

		gen10_5 = new Reference(BookOfBible.Genesis, 10, 5);
		rev11_1 = new Reference(BookOfBible.Revelation, 11, 1);
	}

	// Tests if one of the constructors work
	@Test
	public void testReferenceStringintint() {
		assertTrue(gen1_5.getBook().equals("Genesis"));
		assertEquals(1, gen1_5.getChapter());
		assertEquals(5, gen1_5.getVerse());
		assertTrue(gen2_6.getBook().equals("Genesis"));
		assertEquals(2, gen2_6.getChapter());
		assertEquals(6, gen2_6.getVerse());
	}

	// Tests if the other constructor works
	@Test
	public void testReferenceBookOfBibleintint() {
		assertTrue(gen5_2.getBook().equals("Genesis"));
		assertEquals(5, gen5_2.getChapter());
		assertEquals(2, gen5_2.getVerse());
	}

	// Tests if the equals method returns false when it should
	@Test
	public void testEqualsFalse() {
		// just book is the same
		assertFalse(gen1_5.equals(gen2_6));
		// just chapter is the same
		assertFalse(gen5_2.equals(rev5_3));
		// just verse is the same
		assertFalse(gen2_10.equals(rev3_10));
		// just book is different
		assertFalse(gen1_6.equals(rev1_6));
		// just chapter is different
		assertFalse(rev6_2.equals(rev7_2));
		// just verse is different
		assertFalse(rev2_10.equals(rev2_12));
		// just everything is different
		assertFalse(gen10_5.equals(rev11_1));
	}
	
	// Tests if the equals method is true when it should
	@Test
	public void testEqualsTrue() {
		// a different object returns true for equals method
		Reference testGen1_5 = new Reference(BookOfBible.Genesis, 1, 5);
		assertTrue(gen1_5.equals(testGen1_5));
		// the trivial case
		assertTrue(gen1_5.equals(gen1_5));
	}

	// Tests if the compareTo method works for all cases
	@Test
	public void testcompareTo() {
		// just book is the same
		assertTrue(gen1_5.compareTo(gen2_6) < 0);
		assertTrue(gen2_6.compareTo(gen1_5) > 0);
		// just chapter is the same
		assertTrue(gen5_2.compareTo(rev5_3) < 0);
		assertTrue(rev5_3.compareTo(gen5_2) > 0);
		// just verse is the same
		assertTrue(gen2_10.compareTo(rev3_10) < 0);
		assertTrue(rev3_10.compareTo(gen2_10) > 0);
		// just book is different
		assertTrue(gen1_6.compareTo(rev1_6) < 0);
		assertTrue(rev1_6.compareTo(gen1_6) > 0);
		// just chapter is different
		assertTrue(rev6_2.compareTo(rev7_2) < 0);
		assertTrue(rev7_2.compareTo(rev6_2) > 0);
		// just verse is different
		assertTrue(rev2_10.compareTo(rev2_12) < 0);
		assertTrue(rev2_12.compareTo(rev2_10) > 0);
		// just everything is different
		assertTrue(gen10_5.compareTo(rev11_1) < 0);
		assertTrue(rev11_1.compareTo(gen10_5) > 0);
		// everything is the same
		assertTrue(gen10_5.compareTo(gen10_5) == 0);
	}

	// Tests if the toString method works for 3 cases
	@Test
	public void testToString() {
		assertTrue(rev2_10.toString().equals("Revelation 2:10"));
		assertTrue(gen1_5.toString().equals("Genesis 1:5"));
		assertTrue(gen5_2.toString().equals("Genesis 5:2"));
	}

	// Tests if the getVerse method works for 3 cases
	@Test
	public void testGetVerse() {
		assertEquals(5, gen1_5.getVerse());
		assertEquals(10, rev2_10.getVerse());
		assertEquals(2, gen5_2.getVerse());
	}

	// Tests if the getChapter method works for 3 cases
	@Test
	public void testGetChapter() {
		assertEquals(1, gen1_5.getChapter());
		assertEquals(2, rev2_10.getChapter());
		assertEquals(5, gen5_2.getChapter());
	}

	@Test
	public void testGetBookOfBible() {
		assertEquals(BookOfBible.Genesis, gen1_5.getBookOfBible());
		assertEquals(BookOfBible.Revelation, rev2_10.getBookOfBible());
		assertEquals(BookOfBible.Genesis, gen5_2.getBookOfBible());
	}

	// Tests if the hashcode is the same when it should
	@Test
	public void testHashCodeSame() {
		assertEquals(gen1_5.hashCode(), gen1_5.hashCode());
	}

	// Tests if the hashcode is different when it should
	@Test
	public void testHashCodeDifferent() {
		// just book is the same
		assertFalse(gen1_5.hashCode() == gen2_6.hashCode());
		// just chapter is the same
		assertFalse(gen5_2.hashCode() == rev5_3.hashCode());
		// just verse is the same
		assertFalse(gen2_10.hashCode() == rev3_10.hashCode());
		// just book is different
		assertFalse(gen1_6.hashCode() == rev1_6.hashCode());
		// just chapter is different
		assertFalse(rev6_2.hashCode() == rev7_2.hashCode());
		// just verse is different
		assertFalse(rev2_10.hashCode() == rev2_12.hashCode());
		// just everything is different
		assertFalse(gen10_5.hashCode() == rev11_1.hashCode());
		// a different object returns true for equals method
		Reference testGen1_5 = new Reference(BookOfBible.Genesis, 1, 5);
		assertTrue(gen1_5.hashCode() == testGen1_5.hashCode());
		// the trivial case
		assertTrue(gen1_5.hashCode() == gen1_5.hashCode());
	}

	@After
	public void tearDown() throws Exception {
	}
}
